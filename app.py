from aws_cdk import (
    core,
    aws_ec2 as ec2
)

class VpcStack(core.Stack):
    def __init__(self, app: core.App, stack_name: str, **kwargs):
        super().__init__(app, stack_name, **kwargs)

        subpriv = ec2.SubnetConfiguration(
                              name='privatejh',
                              subnet_type=ec2.SubnetType.PRIVATE,
                              cidr_mask=18
                          )

        subpub = ec2.SubnetConfiguration(
                              name='ingress',
                              subnet_type=ec2.SubnetType.PUBLIC,
                              cidr_mask=18
                          )


        vpc = ec2.Vpc(self, "theVPC",
                      cidr="10.1.0.0/16",
                      subnet_configuration=[subpub, subpriv],
                      max_azs=3
                      )


Env = core.Environment(
    region='ap-southeast-2'
)
app = core.App()
VpcStack(app, "VpcTestStack", env=Env)




